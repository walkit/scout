# Scout

## API сервисов

---

### html-render

```http
POST /json
```

```http
POST /html
```

Принимает конфиг:

```json
{
    "url":"http://example.com",
    "options":{
        "randomUserAgent": true,
        "blockScript": true,
        "blockImages": true,
        "blockCss": true,
        "blockMedia": true,
        "debugRequest": true,
        "useProxy": true
    }
}
```

Возвращает либо `html`, либо `json` вида:

```json
{
    // Массив выполненных запросов
    "debugLog": [
        "url",
        "url2",
        ...
    ],
    "dom": "<html>...</html>"
}
```

---

### suppliers-manager

```http
POST /prices
```

Принимает для обработки файлы в формате xsl, xslx.

Возвращает либо `200`, либо `502`.

---

### database-manager

```http
POST /suppliers-price
```

Принимает и сохраняет в базу массив данных вида:

```json
[
    {
        "product_name": "Название товара",
        "supplier_name": "Название поставщика",
        "product_price": "777.50"
    },
    {
        "product_name": "Тестовый товар2",
        "supplier_name": "Тестовый поставщик2",
        "product_price": "777.50"
    }
]
```

Возвращает либо `200`, либо `502`.

---

```http
GET /suppliers-price
```

Принимает параметры фильтра и возвращает запись с информацие о товаре поставщика.

Формат фильтра:

```json
{
    "supplier_name": "Название поставщика",
    "product_name": "Название товара"
}
```

Формат возвращаемых данных:

```json
{
    "_id": ObjectID,
    "supplier_name": "Название поставщика",
    "product_name": "Название товара",
    "product_price: "7444"
}

```

---

```http
GET /suppliers-name
```

Возвращает массив уникальных имен поставщиков.
Формат возвращаемых данных:

```json
[
    "Название поставщика",
    "Название поставщика",
    ...
]
```

---

```http
POST /parser-config
```

Принимает и сохраняет в базу настройки парсера.
Формат принимаемых настроек:

```json
{
    "name": "СтройМастер",
    "domain": "stroymaster.shop",
    "titleSelector": "h1",
    "priceSelector": "p.new_price",
    "options": {
        "randomUserAgent": true,
        "blockImages": true,
        "blockCss": true,
        "blockScript": true,
        "blockMedia": true,
        "useProxy": false
    }
}
```

Возвращает либо `200`, либо `502`.

---

```http
GET /parser-config
```

Возвращает конфиг парсеров или определенного парсера по домену:
Формат запроса для получения одного конфига:

```json
{
    "domain":"example.com"
}
```

Формат возращаемых данных:

```json
// Для запроса по домену.
// Если запрос без домена, то возвращает массив с теми же данными, что и запрос с доменом.
{
    "options": {
      "randomUserAgent": true,
      "blockImages": true,
      "blockScript": true,
      "blockMedia": true,
      "blockCss": true,
      "showBrowser": true
    },
    "_id": "5f980a57da5239a9d806c8b6",
    "domain": "example.com",
    "__v": 0,
    "name": "Название парсера",
    "priceSelector": "span.price-value",
    "titleSelector": "h1"
}
```

---

```http
POST /product-config
```

Принимает и сохраняет в базу конфиг товара в формате:

```json
{
    "product_url":"Ссылка на карточку товара в нашем магазине",
    "suppliers_info":[
        {
            "supplier_name":"Поставщик1",
            "product_name":"Товар №4"
        },
        ...
    ],
    "competitors_url":[
        "Ссылка на товар у конкурента №1",
        "Ссылка на товар у конкурента №2",
        ...
    ]
}
```

Возвращает либо `200`, либо `502`.

---

```http
GET /product-configs
```

Возвращает все конфиги товара.
Формат ответа:

```json
[
  {
    "suppliers_info": [
      {
        "supplier_name": "Поставщик1",
        "product_name": "Товар поставщика №1"
      }
    ],
    "competitors_url": [
      "Ссылка на товар конкурента1",
      ...
    ],
    "_id": "5fa13a3fdbbf081dbc7bd642",
    "product_url": "Ссылка на наш товар",
    "__v": 0
  },
  ...
]
```

---

```http
POST /product-data
```

Принимает собраные парсером данные товара в формате:

```json
{
  "product_url": "Ссылка на товар",
  "product_name": "Название товара",
  "product_price": 1750
}
```

Возвращает либо `200`, либо `502`.

---

```http
GET /product-data
```

Вовзращает сохраненные данные о товаре, собранные парсером.
Формат запроса:

```json
{
    "product_url": "Ссылка на товар"
}
```

Формат ответа:

```json
{
  "product_url": "ссылка на товар",
  "product_name": "Название товара",
  "product_price": 1750
}

```

---

```http
POST /products-info
```

```json
{
  "product_url": "Ссылка на наш товар",
  "product_name": "Название у нас на сайте",
  "product_price": 16330,
  "suppliers_info": [
    {
      "supplier_name": "Имя поставщика",
      "product_name": "Название товара у поставщика",
      "product_price": 12363
    },
    ...
  ],
  "competitors_info": [
    {
      "product_url": "Ссылка на товар конкурента",
      "product_name": "Название товара конкурента",
      "product_price": 16330
    },
    ...
  ],
  "analytics": {
    "name": "normal_product",
    "description": "Товар нормальный.",
    "solution": "Действий не требуется"
  }
}
```

---

```http
GET /products-info
```

---

### Spiders-manager

```http
POST /check-parser-config
```

```json
{
    "url":"https://www.vseinstrumenti.ru/uborka/pylesosy/dlya-doma/dlya-suhoj-uborki/karcher/vc-3-1-198-125/",
    "parserConfig": {
        "name": "СтройМастер",
        "domain": "stroymaster.shop",
        "titleSelector": "h1",
        "priceSelector": "span.current",
        "options": {
            "randomUserAgent": true,
            "blockImages": true,
            "blockCss": true,
            "blockScript": false,
            "blockMedia": true
        }
    }
}
```
