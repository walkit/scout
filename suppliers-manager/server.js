const express = require('express');
const multer = require('multer');
const bodyParser = require('body-parser');
const fs = require('fs');
const axios = require('axios');
const parser = require('./parser');


const corsDomain = process.env.CORS_DOMAIN || "*"
const dbManagerHost = process.env.DB_MANAGER_HOST || "localhost"
const dbManagerPort = process.env.DB_MANAGER_PORT || 3000
const saveSuppliersDataURL = `http://${dbManagerHost}:${dbManagerPort}/suppliers-price`


const upload = multer({ dest: 'uploads/' });
const app = express();

app.use(bodyParser.json());

// CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", corsDomain); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


// TODO: добавить логирование загрузки прайсов
app.post('/prices', upload.array('prices'), async (req, res) => {
    console.log('Принят запрос на обработку прайсов.');

    let files = req.files;
    console.log(`Количество файлов в запросе - ${files.length}.`);

    if (!files) {
        res.sendStatus(400);
        return;
    }

    // TODO: узкое место. Переделать на async
    try {
        for (const file of files) {
            const parsedPrice = parser.parseXlsx(file);
            await axios.post(saveSuppliersDataURL, parsedPrice);
            fs.unlinkSync(file.path);
        }
    } catch (e) {
        console.log(`В процессе обработки прайсов возникла ошибка: ${e}`)
        res.status(502);
        res.send(e);
        return;
    }
    console.log('Запрос на обработку выполнен.')
    res.sendStatus(200);
})

app.listen(3000, () => {
    console.log('Запущен на 3000 порту.')
});
