const xslx = require('xlsx');

// TODO: добавить логирование разбора прайсов

let parseXlsx = (fileInfo) => {
    console.log('Начинаю обработку прайса.')
    const price = xslx.readFile(fileInfo.path);

    const sheetName = price.SheetNames[0];
    const sheet = price.Sheets[sheetName]

    const productNameColumn = 'A';
    const productPriceColumn = 'B';

    const supplier_name = fileInfo.originalname.replace('.xlsx', '').replace('.xls', '');
    let row = 0;
    let emptyLineCounter = 0;
    let data = []

    while (true) {
        row++;

        const nameCellIndex = `${productNameColumn}${row}`;
        const priceCellIndex = `${productPriceColumn}${row}`;

        const name = sheet[nameCellIndex];
        const price = sheet[priceCellIndex];

        if (name && price) {
            data.push({
                supplier_name,
                product_name: name.v.trim(),
                product_price: price.v
            })
            emptyLineCounter = 0;
        } else {
            emptyLineCounter++;
        }

        if (emptyLineCounter > 5) {
            break;
        }
    }
    console.log(`Прайс ${supplier_name} обработан. Количество позиций - ${data.length}`);
    return data
}

module.exports = {
    parseXlsx
}