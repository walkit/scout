// const MINUTE = 60000;
// const HOUR = MINUTE * 60;
const DELAY_SEED = process.env.SPIDER_LAUNCH_DELAY_RANGE_MS || 60000 * 60 * 2; //60000 * 60 * 2;

// получает количество мс до конца дня
const getMillisecondsToEndDay = () => {
    console.log('Вычисляю количество миллисекунд, оставшихся до окнца дня.')
    const actualDate = new Date();
    const endOfDayDate = new Date(actualDate.getFullYear()
        , actualDate.getMonth()
        , actualDate.getDate()
        , 23, 59, 59);

    return endOfDayDate.getTime() - actualDate.getTime();
};

const makeRandomStartDelay = () => {
    console.log('Вычисляю рандомную задержку перед стартом сбора данных.')
    const timeToEndDay = getMillisecondsToEndDay();

    const delay = Math.floor(Math.random() * (DELAY_SEED * 1.5 - -(DELAY_SEED) + 1) + -(DELAY_SEED));
    return timeToEndDay + delay;
};


const wait = async () => {
    console.log('Запускаю режим ожидания перед стартом сбора данных.')
    const time = makeRandomStartDelay();
    await new Promise(resolve => setTimeout(resolve, time));
}

module.exports = {
    wait
}
// (async () => {

//     console.log(makeRandomStartDelay());
    // while (true) {
    //     const startTime = makeStartTimeTest();
    //     await wait(startTime);
    //     job();
    // }
    // let promiseArray = [() => asyncFuncion(1), () => asyncFuncion(2)];
    // Promise.allSettled(promiseArray.map(promise => promise()));
    // const timeToEndDay = getTimeToEndDay();
    // const interval = makeInterval(-(DELAY), DELAY, timeToEndDay);
    // console.log(makeRandomStartDelay());
// })()