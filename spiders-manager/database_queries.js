const axios = require('axios');

// TODO: сделать более изящно путь к базе
const databaseManagerHost = process.env.DATABASE_MANAGER_HOST || 'localhost';
const databaseManagerPort = process.env.DATABASE_MANAGER_PORT || '3000';
const dbURL = `http://${databaseManagerHost}:${databaseManagerPort}`
// console.log(dbURL);

// Получает все конфиги товаров из базы
const getProductConfigs = async () => {
    return await axios.get(`${dbURL}/product-configs`)
        .then(response => response.data)
        .catch(e => console.log(e))
}

// Получает все конфиги парсеров из базы
const getParserConfigs = async () => {
    return await axios.get(`${dbURL}/parser-config`)
        .then(response => parserConfigs = response.data)
        .catch(e => console.log(e))
}

// Сохраняет собраные данные о товаре в базу
const sendProductDataToDatabaseManager = async (data) => {
    // console.log(data)
    if (data) {
        axios.post(`${dbURL}/product-data`, data)
            .catch(e => console.log(e));
    }
}

module.exports = {
    getParserConfigs,
    getProductConfigs,
    sendProductDataToDatabaseManager
}