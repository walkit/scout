const axios = require('axios');
const { response } = require('express');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;
const db = require('./database_queries');

const htmlRenderHost = process.env.HTML_RENDER_HOST || 'localhost';
const htmlRenderPort = process.env.HTML_RENDER_PORT || '4000';
const minDelayBeforeRequest = process.env.MIN_DELAY_BEFORE_REQUEST_MS || 2000;
const maxDelayBeforeRequest = process.env.MAX_DELAY_BEFORE_REQUEST_MS || 5000;

const renderURL = `http://${htmlRenderHost}:${htmlRenderPort}/html`;
const renderURLJson = `http://${htmlRenderHost}:${htmlRenderPort}/json`;
// const saveProductDataURL = `http://${databaseManagerHost}:${databaseManagerPort}/product-data`


const getHTMLFromURL = async (url, parserOptions) => {
    console.log('Делаю запрос к html-render для получения dom дерева.')
    const renderOptions = {
        url,
        options: parserOptions
    };

    const html = await axios.post(renderURL, renderOptions).then(response => response.data);

    return html;
}

const getJSONFromURL = async (url, parserOptions) => {
    console.log('Делаю запрос к html-render для получения dom дерева в JSON формате');
    parserOptions.debugRequest = true;
    const renderOptions = {
        url,
        options: parserOptions
    };
    const result = await axios.post(renderURLJson, renderOptions).then(response => {
        // console.log(Object.keys(response));
        return response.data
    });
    // console.log(Object.keys(result));
    return result;
}

const getDataFromHTML = async (html, titleSelector, priceSelector, url) => {
    console.log('Извлекаю данные товара из полученного dom дерева.');
    const dom = new JSDOM(html);

    let productTitle = "";
    let productPrice = 0;

    try {
        productTitle = dom.window.document.querySelector(titleSelector).textContent.trim();
    } catch (e) {
        console.log(`Ошибка парсинга названия: ${url}`);
    }

    try {
        const priceRegexp = /\s|\,|₽|:|(руб.)|\/|[а-яА-Я]|\.$/gm;
        const rawProductPrice = dom.window.document.querySelector(priceSelector).textContent.replace(priceRegexp, '');
        console.log(`Строка цены товара: ${rawProductPrice}`);
        productPrice = Number(rawProductPrice);
    } catch (e) {
        console.log(`Ошибка парсинга цены: ${url}`)
        console.log(JSON.stringify(e));
    }

    if (productTitle !== "" || productPrice !== 0) {
        return {
            product_url: url,
            product_name: productTitle,
            product_price: productPrice
        }
    }
}

const randomDelay = async () => {
    console.log('Вычисляю рандомную задержку перед запросом к серверу конкурента.')
    const delay = Math.floor(Math.random() * (maxDelayBeforeRequest - minDelayBeforeRequest + 1) + minDelayBeforeRequest);
    console.log('Жду вычесленную задержку перед запросом к серверу конкурента.')
    await new Promise(resolve => setTimeout(resolve, delay));
}

const run = async (parserConfig, isTest) => {
    console.log('Начинаю выполнение задачи сбора данных с серверов конкурентов.')
    for (let url of parserConfig.urls) {
        console.log(`Извлекаю данные из: ${url}`);
        if (!isTest) {
            await randomDelay();
        }
        const html = await getHTMLFromURL(url, parserConfig.options);
        const data = await getDataFromHTML(html, parserConfig.titleSelector, parserConfig.priceSelector, url);


        if (isTest) {
            console.log('Выполнен тест работы конфига парсера.')
            return data;
        } else {
            console.log("Отправляю результат в базу данных.");
            await db.sendProductDataToDatabaseManager(data);
        }
    }
}

const testRun = async (parserConfig, url) => {
    console.log('Начинаю выполнять тест парсера');
    const { dom, debugLog } = await getJSONFromURL(url, parserConfig.options);
    // console.log(debugLog);
    const data = await getDataFromHTML(dom, parserConfig.titleSelector, parserConfig.priceSelector, url);

    return {
        data,
        debugLog
    }
}

module.exports = {
    run,
    testRun
}