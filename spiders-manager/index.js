const express = require('express');
const bodyParser = require('body-parser');
const spidersArmy = require('./spidersArmy');

const app = express();

const corsDomain = process.env.CORS_DOMAIN || "*"
const port = process.env.PORT || 3000

app.use(bodyParser.json());

// CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", corsDomain); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.post('/check-parser-config', async (req, res) => {
    await spidersArmy.checkParserConfig(req.body.parserConfig, req.body.url)
        .then(data => res.send(JSON.stringify(data)))
        .catch(err => res.send(JSON.stringify(err)));
})

app.listen(port, () => {
    spidersArmy.run();
    console.log(`Запущен на ${port} порту.`);
})