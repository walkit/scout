const urlLib = require('url');
const spider = require('./spider');
const db = require('./database_queries');
const utils = require('./utils');

// Извлекает ссылки для обхода пауками из конфигов товаров и собирает их в массив
const getUrlsFromProductConfig = async (productConfigs) => {
    console.log('Произвожу фомирование массива ссылок на наши товары и товары конкурентов.')
    let urls = [];

    for (let config of productConfigs) {
        urls.push(config.product_url);
        urls.push(...config.competitors_url);
    }

    console.log(`Массив ссылок сформирован. Количество ссылок для обработки: ${urls.length}.`);
    return urls;
}

// Формирует массив заданий для парсинга
const makeTasks = async (parserConfigs, productConfigs) => {
    console.log('Произвожу фомирование массива заданий для парсера.')
    let tasks = [];
    const urls = await getUrlsFromProductConfig(productConfigs);

    for (let parserConfig of parserConfigs) {
        parserConfig.urls = [];
        for (let url of urls) {
            if (urlLib.parse(url).hostname === parserConfig.domain) {
                parserConfig.urls.push(url)
            }
        }
        if (parserConfig.urls.length > 0) {
            tasks.push(parserConfig)
        }
    }

    console.log(`Массив заданий сформирован. Количество заданий: ${tasks.length}.`);
    return tasks;

}

// Формирует массив пауков для асинхронного запуска
const makeSpidersArmy = async (tasks) => {
    console.log('Формирую массив пауков, готовых к асинхронному сбору данных с сайтов конкурентов.')
    let spidersArmy = [];


    for (let task of tasks) {
        spidersArmy.push(() => spider.run(task));
    }
    console.log(`Массив, готовых к сбору данных, пауков сформирован. Количество пауков: ${spidersArmy.length}`);
    return spidersArmy;
}

const run = async () => {
    let endlessloop = true;

    while (endlessloop) {
        console.log('Запускаю новый цикл ежедневного сбора данных с сайтов конкурентов.')

        if (!process.env.DEBUG) {
            await utils.wait();
        }

        const productConfigs = await db.getProductConfigs();
        const parserConfigs = await db.getParserConfigs();

        if (productConfigs.length === 0) {
            console.log('Не найдено конфигураций товаров.');
            continue;
        }

        if (parserConfigs.length === 0) {
            console.log('Не найдено настроеных парсеров.');
            continue;
        }

        const tasks = await makeTasks(parserConfigs, productConfigs);
        const spidersArmy = await makeSpidersArmy(tasks);

        await Promise.allSettled(spidersArmy.map(spider => spider()))
            .then(() => {
                if (process.env.DEBUG) {
                    endlessloop = false;
                }
                console.log('Цикл сбора данных завершен')
            })
            .catch(err => { console.log(JSON.stringify({ error: err })) });

    }
}

const checkParserConfig = async (parserConfig, url) => {
    // console.log(parserConfig);
    // console.log(url);
    // parserConfig.urls = [url];
    const data = await spider.testRun(parserConfig, url);
    return data;
}

module.exports = {
    run,
    checkParserConfig
}

