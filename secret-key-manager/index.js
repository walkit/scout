const express = require('express');
const keyGenerator = require('crypto-random-string');

// секретный ключ
let SECRET_KEY = '';

// сутки
const SECRET_KEY_LIFE_TIME = 60000 * 60 * 24;


const generateSecretKey = () => {
    SECRET_KEY = keyGenerator({ length: 64 })

    console.log('A new secret key has been generated.')
}
generateSecretKey();


const app = express();

app.get('/secret_key', (req, res) => {

    console.log('Private key requested from:', {
        userAgent: req.get('User-Agent'),
        ip: req.connection.remoteAddress.replace('::ffff:', ''),
    })

    res.json({ secret_key: SECRET_KEY })
})


app.listen(3000)

// раз в сутки регенировать ключ
setInterval(() => {
    generateSecretKey()
},
    SECRET_KEY_LIFE_TIME)