const mongoose = require('mongoose');
const fs = require('fs');

const DB_NAME = process.env.SCOUT_DB_NAME || "scout";
const DB_HOST = process.env.SCOUT_DB_HOST || "localhost";
const DB_PORT = process.env.SCOUT_DB_PORT || "27017";

mongoose.connect(`mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).catch(e => {
        console.log(e);
    });


let models = {}
const blackList = ["index"]

fileNames = fs.readdirSync(__dirname, { withFileTypes: true })
    .filter(file => !file.isDirectory())
    .map(file => file.name.replace('.js', ''))
    .filter(name => !blackList.includes(name))



fileNames.forEach(fileName => {
    models[fileName] = require(`${__dirname}/${fileName}`)
})


module.exports = models