const mongoose = require('mongoose');
const Schema = mongoose.Schema;

productConfigSchema = new Schema({
    product_name: String,
    product_url: String,
    suppliers_info: Array,
    competitors_url: Array
})
productConfig = mongoose.model('product_config', productConfigSchema);

// сохраняет конфигурацию товара в базу
const saveProductConfig = async (config) => {
    await productConfig.findOneAndUpdate(
        { product_url: config.product_url },
        config,
        { useFindAndModify: true, upsert: true },
        function (err, doc) {
            if (err) {
                return err
            } else {
                return doc
            }
        }
    )
}

const getProductConfigs = async () => {
    return await productConfig.find({}, function (err, data) {
        if (!err) {
            return data
        } else {
            return err
        }
    })
}

module.exports = {
    saveProductConfig,
    getProductConfigs
}