const mongoose = require('mongoose');
const Schema = mongoose.Schema;

productDataSchema = new Schema({
    product_url: String,
    product_name: String,
    product_price: Number
})
productData = mongoose.model('product_data', productDataSchema)

const saveProductData = async (data) => {
    await productData.findOneAndUpdate(
        { product_url: data.product_url },
        data,
        { useFindAndModify: true, upsert: true },
        function (err, doc) {
            if (err) {
                return err
            } else {
                return doc
            }
        }
    )
}

const getProductData = async (filter) => {

    // status
    if (!filter) filter = {};

    return await productData.findOne(filter, function (err, data) {
        if (!err) {
            // console.log(data)
            return data
        } else {
            return err
        }
    })
}

module.exports = {
    saveProductData,
    getProductData
}