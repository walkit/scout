const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// модель документа с данными о товарах поставщиков 
suppliersPriceSchema = new Schema({
    supplier_name: String,
    product_name: String,
    product_price: Number
})
suppliersPrice = mongoose.model('supplier_price', suppliersPriceSchema);


const saveSuppliersPrice = async (prices) => {
    await suppliersPrice.bulkWrite(
        prices.map((price) => ({
            updateOne: {
                filter: { product_name: price.product_name, supplier_name: price.supplier_name },
                update: { $set: price },
                upsert: true,
                useFindAndModify: true
            }
        })
        ))
}

const getSupplierPrice = async (filter) => {
    // console.log('get supplier price');
    if (!filter) filter = {};
    // console.log(filter);

    return await suppliersPrice.findOne(filter, function (err, data) {
        if (!err) {
            console.log(data)
            return data
        } else {
            return err
        }
    })
}

// TODO: переделать на отдачу максимальной статистики о сохраненных поставщиках. 
const getSuppliersName = () => {
    return suppliersPrice.distinct('supplier_name');
}

module.exports = {
    saveSuppliersPrice,
    getSupplierPrice,
    getSuppliersName
}