const mongoose = require('mongoose');
const Schema = mongoose.Schema;

productInfoSchema = new Schema({
    product_url: String,
    product_name: String,
    product_price: Number,
    suppliers_info: Array,
    competitors_info: Array,
    analytics: Schema.Types.Mixed
})
productInfo = mongoose.model('product_info', productInfoSchema)


const saveProductsInfo = async (data) => {
    // console.log(data);
    await productInfo.findOneAndUpdate(
        { product_url: data.product_url },
        data,
        { useFindAndModify: true, upsert: true },
        function (err, doc) {
            if (err) {
                return err
            } else {
                return doc
            }
        }
    )
}

const getProductsInfo = async (status) => {
    let filter = {};
    if (status) {
        filter = { 'analytics.status': status }
    }

    return await productInfo.find(filter, function (err, data) {
        if (!err) {
            return data
        } else {
            return err
        }
    })
}

module.exports = {
    saveProductsInfo,
    getProductsInfo
}