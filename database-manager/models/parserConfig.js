const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// модель документа с настройками парсеров
parserConfigSchema = new Schema({
    name: String,
    domain: String,
    titleSelector: String,
    priceSelector: String,
    options: {
        randomUserAgent: Boolean,
        blockImages: Boolean,
        blockScript: Boolean,
        blockMedia: Boolean,
        blockCss: Boolean,
        useProxy: Boolean,
        blockList: Array,
        pageLoadTimeout: Number
    }
})
parserConfig = mongoose.model('parser_config', parserConfigSchema);

// сохраняет конфигурацию парсера в базу
const saveParserConfig = async (config) => {
    await parserConfig.findOneAndUpdate(
        { domain: config.domain },
        config,
        {
            upsert: true,
            new: true,
            setDefaultsOnInsert: true,
            rawResult: true,
        },
        function (err, doc) {
            if (err) {
                return err
            } else {
                return doc
            }
        }
    )
}

// Возвращает либо один, либо все конфиги парсера.
const getParserConfig = async (query_domain) => {

    const filter = {};
    if (query_domain) {
        filter = { domain: query_domain }
    }

    return await parserConfig.find(filter, function (err, data) {
        if (!err) {
            return data
        } else {
            return err
        }
    })
}

const deleteParserConfig = async (id) => {
    return await parserConfig.findByIdAndDelete(id);
}


module.exports = {
    saveParserConfig,
    getParserConfig,
    deleteParserConfig
}