const express = require('express');
const models = require('./models');

const corsDomain = process.env.CORS_DOMAIN || '*';
const reqSizeLimit = process.env.REQ_SIZE_LIMIT || '25mb';

const app = express();
app.use(express.json({ limit: reqSizeLimit }));


// CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", corsDomain); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// типа логгер
// app.use(function (req, res, next) {
//     console.log(req);
//     next();
// })

// сохраняет в базу прайсы поставщиков
app.post('/suppliers-price', (req, res) => {
    req.setTimeout(0);
    console.log(req.body);
    models.supplierPrice.saveSuppliersPrice(req.body)
        .then(() => res.sendStatus(200))
        .catch((error) => {
            console.log(error);
            res.sendStatus(502);
        })
});

// отдает цены поставщиклв
app.get('/suppliers-price', async (req, res) => {
    // console.log(req.query);
    await models.supplierPrice.getSupplierPrice(req.query)
        .then(data => res.json(data))
        .catch(err => res.send(err))
});

app.get('/suppliers-name', (req, res) => {
    models.supplierPrice.getSuppliersName()
        .then(data => {
            res.status(200);
            res.json(data);
        })
        .catch(err => {
            console.log(err);
            res.status(502);
            res.send(err)
        })
})

// сохраняет конфиг парсера в базу
app.post('/parser-config', (req, res) => {
    models.parserConfig.saveParserConfig(req.body)
        .then(() => res.sendStatus(200))
        .catch((error) => {
            console.log(error);
            res.sendStatus(502);
        })
})

app.get('/parser-config', (req, res) => {
    // console.log(req.query);
    // TODO: убедиться, что параметр domain приходит в body
    models.parserConfig.getParserConfig(req.query.domain)
        .then(result => {
            // console.log(result)
            res.json(result)
        })
        .catch(error => console.log(error))
})

app.delete('/parser-config/:id', (req, res) => {
    console.log(req.params.id);
    if (req.params.id) {
        models.parserConfig.deleteParserConfig(req.params.id)
            .then(() => res.sendStatus(200))
            .catch(() => res.sendStatus(502))
    } else {
        res.sendStatus(502)
    }
})



// TODO: отрефакторить эндпоинт
app.post('/product-config', (req, res) => {
    // console.log(req.body)
    models.productConfig.saveProductConfig(req.body)
        .then(() => res.sendStatus(200))
        .catch((error) => {
            console.log(error);
            res.sendStatus(502);
        })
})

app.get('/product-configs', (req, res) => {
    models.productConfig.getProductConfigs()
        .then(data => res.json(data))
        .catch(err => res.send(err))
})

app.post('/product-data', (req, res) => {
    console.log('Получен запрос на сохранение результатов парсинга.');
    console.log(req.body);
    models.productData.saveProductData(req.body)
        .then(() => res.sendStatus(200))
        .catch((error) => {
            console.log(error);
            res.sendStatus(502);
        })
})

app.get('/product-data', async (req, res) => {
    // console.log(req.query);
    await models.productData.getProductData(req.query)
        .then(data => res.json(data))
        .catch(err => res.send(err))
})

// TODO: переделать имя endpoint
app.post('/products-info', async (req, res) => {
    await models.productInfo.saveProductsInfo(req.body)
        .then(() => res.sendStatus(200))
        .catch((error) => {
            console.log(error);
            res.sendStatus(502);
        })
})

app.get('/products-info', async (req, res) => {
    // console.log(req.body);
    await models.productInfo.getProductsInfo(req.query.status)
        .then(data => {
            // console.log(data);
            res.json(data);
        })
        .catch(err => res.send(err))
})

app.listen(3000, () => {
    console.log("Запущен на 3000 порту.")
});

