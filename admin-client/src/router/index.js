import Vue from "vue";
import VueRouter from "vue-router";
import Monitoring from "../views/Monitoring.vue";
import NewProduct from "../views/NewProduct.vue";
import NewUser from "../views/NewUser.vue";
import UploadPrices from "../views/UploadPrices.vue";
import NewParser from "../views/NewParser.vue";
import ParsersInfo from "../views/ParsersInfo.vue";

Vue.use(VueRouter);

const routes = [
	{
		path: "/",
		name: "Monitoring",
		component: Monitoring,
	},
	{
		path: "/new-product",
		name: "NewProduct",
		component: NewProduct,
	},
	{
		path: "/new-user",
		name: "NewUser",
		component: NewUser,
	},
	{
		path: "/upload-prices",
		name: "UploadPrices",
		component: UploadPrices,
	},
	{
		path: "/new-parcer",
		name: "NewParser",
		component: NewParser,
	},
	{
		path: "/parsers-info",
		name: "ParsersInfo",
		component: ParsersInfo,
	},

];

const router = new VueRouter({
	routes,
});

export default router;
