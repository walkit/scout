import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        // НАСТРОЙКИ
        config: {
            priceUploadURL: 'https://scout.walkit.tk/suppliers-manager/prices',
            parserConfigUploadURL: 'https://scout.walkit.tk/database-manager/parser-config',
            productConfigUploadURL: 'https://scout.walkit.tk/database-manager/product-config',
            suppliersNameURL: 'https://scout.walkit.tk/database-manager/suppliers-name',
            checkParserConfigURL: 'https://scout.walkit.tk/spiders-manager/check-parser-config'
        },
        // состояние лоадера: false - скрыт, true - отображен
        isActiveLoader: false,
        // состояния окна сообщений
        isActiveNotify: false,
        notifyTitle: 'Title',
        notifyText: 'Text',
        notifyType: 'info' //['info','success','warn','error']
    },

    getters: {
        // отдает конфиг
        config: state => {
            return state.config;
        },
        // отдает состояние лоадера
        loaderState: state => {
            return state.isActiveLoader;
        },
        // отдает состояние окна сообщений
        notifyState: state => {
            return {
                isActive: state.isActiveNotify,
                type: state.notifyType,
                title: state.notifyTitle,
                text: state.notifyText
            }
        }
    },

    mutations: {
        // меняет состояние лоадера
        changeLoaderState(state) {
            state.isActiveLoader = !state.isActiveLoader;
        },
        // меняет содержимое и тип уведомления
        setNotifyBody(state, payload) {
            state.notifyType = payload.type;
            state.notifyTitle = payload.title;
            state.notifyText = payload.text;
        },
        // переключает скрытие\отображение уведомления
        changeNotifyState(state) {
            state.isActiveNotify = !state.isActiveNotify;
        }
    },

    actions: {
        // использовать для запуска смены состояния лоадера в компонентах
        toggleActiveLoader({ commit }) {
            commit('changeLoaderState')
        },
        // отобразить уведомление и скрыть по таймеру
        showNotify({ commit }, payload) {
            commit('setNotifyBody', payload);
            commit('changeNotifyState');
            setTimeout(() => {
                commit('changeNotifyState');
            }, 10000);
        }
    },

    modules: {
    }
})
