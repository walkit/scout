deploy:
	make build-all-images
	make push-all-images-to-registry
	scp docker-compose.yml m@io.vpn:/home/m/scout/
	ssh m@io.vpn 'cd /home/m/scout && docker-compose pull'
	ssh m@io.vpn 'cd /home/m/scout && docker-compose up -d'

dev-deploy:
	make build-all-images
	docker-compose up -d

admin-client-serve:
	cd admin-client && yarn serve
admin-client-build:
	cd admin-client && yarn build
admin-client-build-docker-image:
	cd admin-client && docker build -t walkit/admin-client:latest .
admin-client-push-image-to-registry:
	docker push walkit/admin-client:latest


build-all-images:
	make secret-key-manager-build-docker-image
	make html-render-build-docker-image
	make suppliers-manager-build-docker-image
	make database-manager-build-docker-image
	make spiders-manager-build-docker-image
	make monitor-manager-build-docker-image
	make admin-client-build-docker-image

	
push-all-images-to-registry:
	make secret-key-manager-push-image-to-registry
	make html-render-push-image-to-registry
	make suppliers-manager-push-image-to-registry
	make database-manager-push-image-to-registry
	make spiders-manager-push-image-to-registry
	make monitor-manager-push-image-to-registry
	make admin-client-push-image-to-registry


secret-key-manager-debug:
	cd secret-key-manager && npm run debug
secret-key-manager-build-docker-image:
	cd secret-key-manager && docker build -t walkit/secret-key-server:latest .
secret-key-manager-push-image-to-registry:
	docker push walkit/secret-key-manager:latest


html-render-debug:
	cd html-render && yarn debug
html-render-build-docker-image:
	cd html-render && docker build -t walkit/html-render:latest .
html-render-push-image-to-registry:
	docker push walkit/html-render:latest


suppliers-manager-debug:
	cd suppliers-manager && npm run dev
suppliers-manager-build-docker-image:
	cd suppliers-manager && docker build -t walkit/suppliers-manager:latest .
suppliers-manager-push-image-to-registry:
	docker push walkit/suppliers-manager:latest


database-manager-debug:
	cd database-manager && npm run dev
database-manager-build-docker-image:
	cd database-manager && docker build -t walkit/database-manager:latest .
database-manager-push-image-to-registry:
	docker push walkit/database-manager:latest


spiders-manager-debug:
	cd spiders-manager && DEBUG=1 npm run dev
spiders-manager-build-docker-image:
	cd spiders-manager && docker build -t walkit/spiders-manager:latest .
spiders-manager-push-image-to-registry:
	docker push walkit/spiders-manager:latest


monitor-manager-debug:
	cd monitor-manager && DB_MANAGER_HOST="localhost" DB_MANAGER_PORT=3000 JOB_INTERVAL=10000 npm run dev
monitor-manager-build-docker-image:
	cd monitor-manager && docker build -t walkit/monitor-manager:latest .
monitor-manager-push-image-to-registry:
	docker push walkit/monitor-manager:latest