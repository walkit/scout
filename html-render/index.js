const render = require('./render');

const express = require('express');
const app = express();
app.use(express.json());

const corsDomain = process.env.CORS_DOMAIN || '*';
let PAGE_LOAD_TIMEOUT = process.env.PAGE_LOAD_TIMEOUT || 30000;

const wait = async (delay) => {
    await new Promise(resolve => setTimeout(resolve, delay));
}

const getContent = async (url, options) => {
    let data = `<html><h1>Parsing failed.</h1><p>${url}</p></html>`;
    const page = await render.getPage(options);

    if (options.pageLoadTimeout && options.pageLoadTimeout > 0) {
        PAGE_LOAD_TIMEOUT = options.pageLoadTimeout;
    }

    try {

        console.log(`Делаю запрос по ${url}`);
        await page.goto(url, { timeout: PAGE_LOAD_TIMEOUT });
        console.log(`Страница загружена. ${url}`);

        console.log(`Вытаскиваю dom дерево.`)
        await Promise.race([
            // page.evaluate(() => document.querySelector('*').outerHTML),
            page.content(),
            wait(PAGE_LOAD_TIMEOUT)
        ]).then(result => {
            if (result) {
                data = result;
                console.log(`Dom дерево вытащено`)
            } else {
                console.log(`Не удалось получить контент за ${PAGE_LOAD_TIMEOUT} ms`)
            }
        })

    } catch (err) {
        console.log('Загрузка контента не удалась.');
        console.log(JSON.stringify({ "error": err }));
    }

    console.log(`Закрываю страницу.`)
    await Promise.race([
        page.close(),
        wait(PAGE_LOAD_TIMEOUT)
    ])
        .then(() => {
            console.log(`Страница закрыта.`)
        })
    return { debugLog: options.debugLog, dom: data }
}

// CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", corsDomain); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.post('/json', async (req, res) => {

    console.log('Поступил запрос на рендеринг в json.');
    let options = req.body.options;
    let url = req.body.url;
    const content = await getContent(url, options);
    res.json(content);

})


app.post('/html', async (req, res) => {
    console.log('Поступил запрос на рендеринг в html.');
    let options = req.body.options;
    let url = req.body.url;
    const content = await getContent(url, options);
    res.send(content.dom)
})

app.listen(3000, () => {
    console.log('Запущен на 3000 порту.')
})


