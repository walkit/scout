const db = require('./database_queries');
const analytics = require('./analytics');
const jobInterval = process.env.JOB_INTERVAL || 1000 * 60 * 2; // 2 минуты на тесты

// Заполняет основные данные о товаре
const fillInBasicData = async (productConfig) => {
    console.log('Заполняю основные данные товара.');
    const productInfo = await db.getProductInfo(productConfig.product_url);
    console.log('Основные данные товара заполнены.');
    return {
        product_url: productInfo.product_url,
        product_name: productInfo.product_name,
        product_price: productInfo.product_price,
    };
}

// Заполняет данные поставщиков
const fillInSuppliersData = async (productConfig) => {
    console.log('Заполняю данные поставщиков.');
    let suppliersInfo = [];

    for (let supplier of productConfig.suppliers_info) {
        const supplierPrice = await db.getSupplierPrice(supplier.supplier_name, supplier.product_name);
        suppliersInfo.push({
            supplier_name: supplier.supplier_name,
            product_name: supplier.product_name,
            product_price: supplierPrice
        });
    };
    console.log('Данные поставщиков заполнены.');
    return suppliersInfo;
}

// Заполняет данные конкурентов
const fillInCompetitorsData = async (productConfig) => {
    console.log('Заполняю данные конкурентов.');
    let competitorsInfo = [];

    for (let url of productConfig.competitors_url) {
        const productInfo = await db.getProductInfo(url);
        try {
            competitorsInfo.push({
                product_url: productInfo.product_url,
                product_name: productInfo.product_name,
                product_price: productInfo.product_price
            });
        } catch (e) {
            console.log(`Товар конкурента в базе не найден: ${url}`);
        };
    };
    console.log('Данные конкурента заполнены.')

    return competitorsInfo;
}

// Заполняет полную информацию о продукте
// TODO: обработать пусты массивы
const fillInFullData = async (productConfig) => {
    console.log('заполняю полную информацию о продукте');
    let productInformation = await fillInBasicData(productConfig);
    productInformation.suppliers_info = await fillInSuppliersData(productConfig);
    productInformation.competitors_info = await fillInCompetitorsData(productConfig);
    productInformation.analytics = analytics.doProductAnalysis(productInformation);
    console.log('Информация о продукте заполнена.')
    return productInformation;
}

const job = async () => {
    // Получаем массив сохраненных конфигов товаров
    console.log("Запрашиваю конфигурации товаров.")
    const productConfigs = await db.getProductConfigs();

    if (!productConfigs) {
        console.log('В базе не найдено товаров для выполнения аналитики.')
        return
    }

    console.log(`Получено ${productConfigs.length} конфигураций.`)
    // Для каждого конфига собираем сводную информацию о товаре
    for (let productConfig of productConfigs) {
        // Сбор информации
        let productInformation = await fillInFullData(productConfig);
        // Сохранение собранной инфы в базу для дальнейшего использования
        await db.saveProductsInfoToDatabase(productInformation);
    }
}

(async () => {
    console.log("Монитор запущен.")

    setInterval(async () => {
        console.log("Начинаю выполнять рассчеты.")
        await job()
        console.log("Рассчеты выполнены и сохранены в базу.")
    }, jobInterval);
})()
