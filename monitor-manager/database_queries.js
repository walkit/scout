const axios = require('axios');

// TODO: сделать более изящно путь к базе
const dbManagerHost = process.env.DB_MANAGER_HOST || 'http://localhost'
const dbManagerPort = process.env.DB_MANAGER_PORT || '3000'
const dbURL = `http://${dbManagerHost}:${dbManagerPort}`


// Получает все настройки товаров из базы данных
const getProductConfigs = async () => {
    console.log('Делаю запрос в database-manager на получение всех конфигов товаров.');
    return await axios.get(`${dbURL}/product-configs`)
        .then(response => response.data)
}

// Получает цену товара поставщика по имени юр. лица и названию товара
const getSupplierPrice = async (supplierName, supplierProductName) => {
    console.log('Делаю запрос в database-manager на получение данных поставщика.');
    return await axios.get(`${dbURL}/suppliers-price`, {
        params: { supplier_name: supplierName, product_name: supplierProductName }
    })
        .then(response => {

            if (response.data) {
                return response.data.product_price
            } else {
                return 0
            }
        })
        .catch(err => console.log(err))
}

// Получает информацию о товаре по url товара
const getProductInfo = async (url) => {
    console.log('Делаю запрос в database-manager на получение данных товара.');
    return await axios.get(`${dbURL}/product-data`, {
        params: { product_url: url }
    })
        .then(response => response.data)
        .catch(err => console.log(err))
}


// Сохраняет обработанные данные товара в базу данных
const saveProductsInfoToDatabase = async (productsInfo) => {
    console.log('Делаю запрос в database-manager на сохрание результата анализа товара.');
    await axios.post(`${dbURL}/products-info`, productsInfo)
        .catch(e => console.log(e))

}

module.exports = {
    getProductConfigs,
    getSupplierPrice,
    getProductInfo,
    saveProductsInfoToDatabase
}