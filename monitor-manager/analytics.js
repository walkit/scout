
// Извлекает цены для дальнейшей обработки статуса товара
// Нашу цену, миннимальную цену закупки, наценку, минимальную цену конкурентов
const extractPricesFromProductInformation = (productInformation) => {
    console.log('Извлекаю цены для дальнейшей аналитики товара.');
    // Формирует массив цен поставщиков
    const suppliersPrice = productInformation
        .suppliers_info
        .map(supplier => supplier.product_price);

    // Формирует массив цен конкурентов
    const competitorsPrice = productInformation
        .competitors_info
        .map(competitor => competitor.product_price);

    // Цена у нас на сайте
    const ourPrice = productInformation.product_price;
    // Минимальная цена у поставщиков
    const minSupplierPrice = Math.min(...suppliersPrice);
    // Минимальная цена у конкурентов
    const minCompetitorPrice = Math.min(...competitorsPrice);
    // Наша наценка
    const markup = computeMarkup(ourPrice, minSupplierPrice);

    return {
        ourPrice,
        minSupplierPrice,
        markup,
        minCompetitorPrice
    }
}

// Рассчитывает нашу наценку
const computeMarkup = (ourPrice, supplierPrice) => {
    console.log('Рассчитываю наценку товара.');
    ourPrice / supplierPrice - 1;
}

// Статусы товара
const productStatus = {
    // normal_product - нормальный товар
    normalProduct: {
        status: "normal_product",
        description: "Товар нормальный.",
        solution: "Действий не требуется."
    },
    // expensive_purchase - закупаем дорого
    expensivePurchase: {
        status: "expensive_purchase",
        description: "Товар закупаем дорого.",
        solution: "Необходимо изучить предложения других поставщиков."
    },
    // sell_cheap - продаем дешево
    sellCheap: {
        status: "sell_cheap",
        description: "Товар продаем дешевле минимальной цены конкурентов.",
        solution: "Требуется корректировка цены на товар."
    },
    // sell_expensive - продаем дорого
    sellExpensive: {
        status: "sell_expensive",
        description: "Товар продаем дороже минимальной цены конкурентов.",
        solution: "Требуется корректировка цены на товар."
    },
    // low_markup - низкая наценка
    lowMarkup: {
        status: "low_markup",
        description: "Наценка товара ниже установленного порога.",
        solution: "Необходимо изучить предложения других поставщиков."
    },
    // analysis_error - когда что то пошло не так
    analysisError: {
        status: "analysis_error",
        description: "Не удалось провести анализ товара.",
        solution: "Предоставьте разработчику данные товара и список действий, которые привели к появлению этого статуса."
    }
}


// Проводит анализ продукта
const doProductAnalysis = (productInformation) => {
    console.log('Провожу анализ товара.');
    const productPrices = extractPricesFromProductInformation(productInformation);

    if (isNormalProduct(productPrices)) {
        return productStatus.normalProduct;
    };

    if (isExpensivePurchaseProduct(productPrices)) {
        return productStatus.expensivePurchase;
    };

    if (isSellCheapProduct(productPrices)) {
        return productPrices.sellCheap;
    };

    if (isSellExprensiveProduct(productPrices)) {
        return productStatus.sellExpensive;
    };

    if (isLowMarkupProduct(productPrices)) {
        return productStatus.lowMarkup;
    };

    return productStatus.analysisError;
}

// TODO: избавиться от захардкоженной переменной!!!
// Проверяет, нормальный ли товар
const isNormalProduct = (productPrices) => {
    const p = productPrices;
    return (p.ourPrice > p.minSupplierPrice && p.ourPrice === p.minCompetitorPrice && p.markup >= 0.15);
}

// Проверяет, дорого ли мы закупаем товар
const isExpensivePurchaseProduct = (productPrices) => {
    const p = productPrices;
    return (p.minSupplierPrice >= p.minCompetitorPrice || p.minSupplierPrice >= p.ourPrice);
}

// Проверяет, дешево ли мы продаем товар
const isSellCheapProduct = (productPrices) => {
    const p = productPrices;
    return (p.ourPrice > p.minSupplierPrice && p.ourPrice < p.minCompetitorPrice);
}

// Проверяет, дорого ли мы продаем товар
const isSellExprensiveProduct = (productPrices) => {
    const p = productPrices;
    return (p.ourPrice > p.minSupplierPrice && p.ourPrice > p.minCompetitorPrice)
}

// TODO: избавиться от захардкоженной переменной!!!
// Проверяет, низкая ли наценка на наш товар
const isLowMarkupProduct = (productPrices) => {
    const p = productPrices;
    return (markup < 0.15)
}


module.exports = {
    doProductAnalysis
}